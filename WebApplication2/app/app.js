﻿function elemscrolltop() {
    var height = document.getElementById('chat').scrollHeight + 50;
    document.getElementById('chat').scrollTop = height;
}



var app = angular.module('app', []);
app.controller('homecontroller', function ($scope, $http) {

    

    $scope.chat = $.connection.chatHub;

    $scope.message = { text: '' };
    $scope.messages = [];
    $scope.user = { name: ''};
    $scope.users = [];
    $scope.userId = {};
    $scope.reg = true;

    if (localStorage.getItem("user") !== null) {
        $scope.user = JSON.parse(localStorage.getItem("user"));
        $scope.reg = false;
    }

    $scope.chat.client.onNewUserConnected = function (id, name) {
        $scope.messages.push(name + " присоединился!");
        elemscrolltop();
    }

    $scope.chat.client.GetMessages = function (message) {
        $scope.messages.push(message);
        elemscrolltop();
        $scope.$apply();
    }

    $scope.chat.client.addMessage = function (message) {
        $scope.messages.push(message);
        elemscrolltop();
        $scope.$apply();
    }

    $scope.chat.client.GetUsersName = function (user) {
        $scope.users.push(user);
        console.log($scope.users);
        $scope.$apply();
    }
    
    $scope.chat.client.onConnected = function (id) {
        $scope.userId = id;
        $scope.$apply();
    }

    $scope.chat.client.onDoubleName = function (error) {
        alert(error);
        localStorage.removeItem("user");
        location.reload();
    }

    $.connection.hub.start().done(function () {

    var init = function () {
        $scope.chat.server.onload();
    }

    init();

        $scope.adduser = function () {

        var initUsers = function () {
            $scope.chat.server.initusers();
        }
        initUsers();
        
        if (localStorage.getItem("user") !== null) {
            console.log(localStorage.getItem("user"), " local store");
            $scope.user = JSON.parse(localStorage.getItem("user"));
            $scope.chat.server.connect($scope.user.name);

        }
        else {
            var costyl = true;
            console.log($scope.users.length);
            for (var i = 0; i < $scope.users.length; i++) {
                console.log($scope.users[i], " dsf");
                if ($scope.users[i] === $scope.user.name) {
                    costyl = false;
                }
            }
            console.log(costyl);
            if (costyl === true) {
                localStorage.setItem("user", JSON.stringify($scope.user));
                $scope.user = JSON.parse(localStorage.getItem("user"));
                $scope.chat.server.connect($scope.user.name);
                $scope.reg = false;
            }
            else {
                alert("Имя занято");
                location.reload();
            }
                
        }
    }
    $scope.sendmessage = function () {
        $scope.chat.server.send($scope.user.name, $scope.message.text);
        $scope.message = {}
    };
    });
});

